import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import Login from "./modules/auth/components/Login";
import Layout from "./modules/layout/layout";
import { useAuth } from "./modules/auth/components/Auth";
import "bootstrap/dist/css/bootstrap.min.css";
import UserList from "./modules/users/components/userList";



const App = () => {
  const {auth} = useAuth();

  // console.log(auth);
  // console.log(auth!==null);

    return(
        <BrowserRouter>
        <Routes>
            {auth!==null ? 
            
            <>
              <Route path="/" element={<Layout />}>
                 {/* <Route path="/" element={<MainLayout/>}/> */}
                 <Route path="userList" element={<UserList />} />
              </Route>
             
            
              <Route path="/*" element={<Navigate to="/" />}></Route>
            </>
            :
            <>
              <Route index path="auth/login" element={<Login />}></Route>
              <Route path="*" element={<Navigate to="/auth/login" />}></Route>
            </>
}
            </Routes>
        </BrowserRouter>
    );
 };

 export default App;