import axios from "axios";

const setAuth = (value) =>
    localStorage.setItem("token", value);

const getAuth = () =>
    localStorage.getItem("token");

const setUpAxios = () => {
    axios.defaults.baseURL = "http://13.214.207.172:6002";
    axios.defaults.headers = {
        "Authorization": `Bearer ${getAuth()}`
    }
}
export { setAuth, getAuth, setUpAxios }