import { useState } from "react";
import { login } from "../core/request";
import { useAuth } from "./Auth";

 
const Login = () => {
    const [username, setUsername] = useState("super admin");
    const [pass, setPass] = useState("123");
    const {saveAuth} = useAuth(); 

    const onLogin = (event) => {
        event.preventDefault(); //ot work teh na kleng niz
        login(username, pass).then((response)=>{
            console.log(response);
            saveAuth(response.data.data.token)
        })
    }
  
    return  <div className="login" >
        {
            <div className="row login-row" >
            <div className="col-md-4 offset-md-4"  >
                {/* <h2 className="text-center text-dark mt-5">Login Form</h2>
                <div className="text-center mb-5 text-dark">Made with bootstrap</div> */}
                <div className="card my-0 py-0" >
                    <form className="card-body" style={{backgroundColor:"#F2D7D5"}}>

                        <div className="text-center">
                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQj66t3FyHrX1Z_WDnvvcvewyJ6iY7NJilOMZkNcf6OgQ&s"
                                 className="img-fluid profile-image-pic img-thumbnail  my-3"
                                 width="50px" alt="profile"/>
                        </div>

                        <div className="container">
                            <h2 className="text-center text-black">Sign In</h2>
                        </div>

                        <div className="mb-3">
                            <h3 className="text-black">Username</h3>
                            <input type="text" className="form-control w-100" id="Username" aria-describedby="emailHelp"
                                   placeholder="User Name" onChange={(e)=>setUsername(e.target.value)} />
                        </div>
                        <div className="mb-3">
                        <h3 className="text-black">Password</h3>
                            <input type="password" className="form-control w-100" id="password" placeholder="password" 
                            onChange={(e)=>setPass(e.target.value)} />
                        </div>
                        <div className="text-center">
                            <button onClick={onLogin} className="btn .btn1 btn-color px-3 mb-3 w-100  text-white" >Login</button>
                        </div>
                        <div className="text-center text-danger"> </div>
                        <div id="emailHelp" className="form-text text-center mb-3 text-white">Not
                            Registered? <a href="/" className="text-white fw-bold"> Create an
                                Account</a>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        }
    </div>;
}
 export default Login;