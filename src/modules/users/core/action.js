import { useDispatch } from "react-redux"
import { getUsers } from "./request"
import { setUsers } from "./reducer";


const useUsers = () => {

    const dispatch = useDispatch();

    // const fetchUsers = () => {
    //     return getUsers().then((users) => {
    //         // dispatch(setUsers(users))
    //     })
    // }

     const fetchUsers = () => {
          getUsers().then((res) => {
              // const  format = res.data.data
              // format.forEach((user) =>{
              //     dispatch(setUsers(user))
              // })
              dispatch(setUsers( res.data.data))
          })

        }


    return {fetchUsers}
}

export { useUsers }