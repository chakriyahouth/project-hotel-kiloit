import React, { useState } from 'react';
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
} from '@ant-design/icons';
import { Layout, Menu, Button, theme } from 'antd';
import { AiOutlineDashboard, } from "react-icons/ai";
import { GiNotebook } from "react-icons/gi";
import { BiSolidCategoryAlt } from "react-icons/bi";
import { IoIosPricetag } from "react-icons/io";
import { FaUser } from "react-icons/fa";
import { Outlet, useNavigate } from 'react-router-dom';
const { Header, Sider, Content } = Layout;

const MainLayout = () =>{
    const navigate = useNavigate ();
    const [collapsed, setCollapsed] = useState(false);
    const {
        token: { colorBgContainer, borderRadiusLG },
      } = theme.useToken();
     return (
        <Layout>
        <Sider trigger={null} collapsible collapsed={collapsed}>
          <div className="demo-logo-vertical" />
          <Menu
            theme="dark"
            mode="inline"
            defaultSelectedKeys={['']}
            onClick={({ key }) => {
              if (key == "signout") {
              } else {
                navigate(key);
              }
            }}
            items={[
              {
                key: 'Dashboard',
                icon: <AiOutlineDashboard />,
                label: 'Dashboard',
              },
              {
                key: 'Blog',
                icon: <GiNotebook />,
                label: 'Blog',
              },
              {
                key: 'Category',
                icon: <BiSolidCategoryAlt />,
                label: 'Category',
              },
              ,
              {
                key: 'Tag',
                icon: <IoIosPricetag />,
                label: 'Tag',
              },
              {
                label: 'ADMINISTRATION'
              },
        
              {
                key: 'userList',
                icon: <FaUser />,
                label: 'User Management',
               
              },
            ]}
          />
        </Sider>
        <Layout>
          <Header
            style={{
              padding: 0,
              background: colorBgContainer,
            }}
          >
            <Button
              type="text"
              icon={collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
              onClick={() => setCollapsed(!collapsed)}
              style={{
                fontSize: '16px',
                width: 64,
                height: 64,
              }}
            />
          </Header>
          <Content
            style={{
              margin: '24px 16px',
              padding: 24,
              minHeight: 280,
              background: colorBgContainer,
              borderRadius: borderRadiusLG,
            }}
          >
            <Outlet />
          </Content>
        </Layout>
      </Layout>
     );
};

export default MainLayout;